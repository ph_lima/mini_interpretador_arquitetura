var editorNosso;
var editorJava;
var editorAssembly;
var editorBinario;

var processarAutomaticamente = false;

function inicializarGUI(){
    editorNosso = CodeMirror.fromTextArea(document.getElementById("Codigo0"), {
            lineNumbers: true,
            matchBrackets: true
    });


    editorNosso.on('change',function(cMirror){
        if(processarAutomaticamente === true){
            processarEntradas();
        }
    });
    
    editorNosso.on('keyHandled', function(cMirror, name, evt){
        if(processarAutomaticamente === false){
            if(evt.keyCode === 13){
                processarEntradas();
            }
        }
    });

    editorJava = CodeMirror.fromTextArea(document.getElementById("Codigo1"), {
            lineNumbers: true,
            matchBrackets: true,
            mode: "text/x-java",
            readOnly: true,
    });


   editorAssembly = CodeMirror.fromTextArea(document.getElementById("Codigo2"), {
           lineNumbers: true,
           mode: {name: "gas", architecture: "ARMv6"},
           readOnly: true,
   });


   editorBinario = CodeMirror.fromTextArea(document.getElementById("Codigo3"), {
           lineNumbers: true,
           lineWrapping: true,
           readOnly: true,
   });
   
   $('#chkProcessarAutomaticamente').change(function() {
       processarAutomaticamente = $(this).is(":checked");
    });   
}

function textoParaBinario(texto){
    var tamanhoTexto = texto.length;
    var dadosBinario = [];    
    for(var i = 0 ; i < tamanhoTexto ; i++){
        dadosBinario.push(texto.charCodeAt(i).toString(2));
    }    
    return dadosBinario.join('');
}

function processarEntradas(){
    var codigoEntrada = $.trim(editorNosso.getValue());
    
    Interpretador.executar(codigoEntrada);
    
    editorJava.setValue(Interpretador.getProgramaJava());
    editorAssembly.setValue(Interpretador.getProgramaAssembly());
    
    editorBinario.setValue(textoParaBinario(codigoEntrada));
}