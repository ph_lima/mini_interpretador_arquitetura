
var eTiposToken = {
    TEMPORARIO          : 0,
    DELIMITADOR         : 1,
    IDENTIFICADOR       : 2,
    NUMERO              : 3,
    PALAVRA_CHAVE       : 4,
    STRING              : 5,
    BLOCO               : 6
};

var eOperador = {
    IGUAL               : '==',
    MENOR_IGUAL         : '<=',
    MENOR               : '<',
    MAIOR               : '>',
    MAIOR_IGUAL         : '>=',
    DIFERENTE           : '!='
};

var PalavraChaveArray = {
    'escreva'           : 0,
    'para'              : 0,
    'fim_para'          : 0,
    'ate'               : 0
};

var Interpretador = {    
    _programa           : '',
    _posicao            : 0,
    _pilhaPosicao       : [],
    _tamanhoPrograma    : 0,
    
    _devDebug           : false,
    _devTrace           : false,
    
    _tipoToken          : eTiposToken.TEMPORARIO,
    _token              : '',
    
    _pilhaToken         : [],
    
    _tokenBuilder       : [],
    _varLocal           : [],
      
    _progJava           : [],
    _progAssembly       : [],
      
    _ptrCode : function(){
        if(this._ehFimPrograma()){
            return 0;
        }
        return this._programa.charCodeAt(this._posicao);
    },
    
    _ehFimPrograma : function(){
        return this._posicao >= this._tamanhoPrograma;       
    },
    
    _tokenParaString : function(){
        return this._tokenBuilder.join('');
    },
    
    _ptr : function(){
        if(this._ehFimPrograma()){
            return '\0';
        }
        return this._programa.charAt(this._posicao);
    },
    
    _proximoPtr : function(){
        if(this._ehFimPrograma() || this.posicao + 1 === this._tamanhoPrograma){
            return '\0';
        }
        return this._programa.charAt(this._posicao + 1);
    },
    
    _incPrtEx : function(qtd){
        if((this._posicao += qtd) > this._tamanhoPrograma){
            this._posicao = this._tamanhoPrograma;
        }
    },
    
    _incPtr : function (){
        if(++this._posicao > this._tamanhoPrograma){
            this._posicao =  this._tamanhoPrograma;
        }
    },
        
    _addToken : function(novoToken){
        this._tokenBuilder.push(novoToken);
    },
       
    _trace : function(){
        if(this._devTrace){
            console.log(arguments);
        }
    },
       
    _log : function(){
        if(this._devDebug){
            console.log(arguments);
        }
    },
    
    _encontrarToken : function(){
        if(this._temPosicaoSalva() === false){
            this._salvarToken();
        }
    
        this._tipoToken     = this._getToken();
        this._token         = this._tokenParaString();
    },
                   
    _getToken : function(){
        this._tokenBuilder = [];        
        this._tipoToken = eTiposToken.TEMPORARIO;
                                
        // Verifica espaçamento
        while(this._ehEspacamento() && this._ptr() !== '\0'){
            this._incPtr();            
        }
                
        if(this._ehAlgumCaracterInformado('\r\n')){
            this._incPtr();
            if(this._ehAlgumCaracterInformado('\r\n')){
                this._incPtr();
            }
            while(this._ehEspacamento() && !this._ehFimPrograma()){
                this._incPtr();
            }
            this._addToken(';');
            return eTiposToken.DELIMITADOR;
        }
                
        // Verifica se encontramos o fim
        if(this._ehFimPrograma()){
            return eTiposToken.DELIMITADOR;
        }       
        
        // Verifica se é ou pode ser um operador relacional
        if(this._ehAlgumCaracterInformado('!<>=')){
            var ehOperadorRelacional = false;
            
            switch(this._ptr()){
                case '=':
                    if(this._proximoPtr() === '='){
                        this._addToken(eOperador.IGUAL);
                        this._incPrtEx(2);                        
                        ehOperadorRelacional = true;
                    }
                    break;
                case '!':
                    if(this._proximoPtr() === '='){
                        this._addToken(eOperador.DIFERENTE);
                        this._incPrtEx(2);
                        ehOperadorRelacional = true;
                    }
                    break;
                case '<':                    
                    if(this._proximoPtr() === '='){
                        this._addToken(eOperador.MENOR_IGUAL);
                        this._incPrtEx(2);            
                    }else{
                        this._addToken(eOperador.MENOR);
                        this._incPtr();
                    }
                    ehOperadorRelacional = true;
                    break;
                case '>':
                    if(this._proximoPtr() === '='){
                        this._addToken(eOperador.MAIOR_IGUAL);
                        this._incPrtEx(2);            
                    }else{
                        this._addToken(eOperador.MAIOR);
                        this._incPtr();
                    }
                    ehOperadorRelacional = true;
                    break;
            }
                        
            if(ehOperadorRelacional){                
                return eTiposToken.DELIMITADOR;
            }
        }
               
        if(this._ehAlgumCaracterInformado('+-*^/%=;(),\'')){
            this._addToken(this._ptr());
            this._incPtr();
            return eTiposToken.DELIMITADOR;
        }       
        
        if(this._ptr() === '"'){            
            this._incPtr();
            
            while(!this._ehFimPrograma()){
                var caracter = this._ptr();
                if(caracter !== '"' && caracter !== '\r' && caracter !== '\n' && caracter !== '\0'){
                    this._addToken(this._ptr());
                    this._incPtr();
                }else{
                    if(caracter === '"'){
                        this._incPtr();
                    }
                    break;
                }
            }
            
            if(!this._ehAlgumCaracterInformado('\r\n\0')){
                this._erroDeSintaxe('String não finializada corretamente !');
            }
            
            return eTiposToken.STRING;
        }
                 
        if(this._ehNumero()){
            while(!this._ehDelimitador()){
                this._addToken(this._ptr());
                this._incPtr();
            }
            return eTiposToken.NUMERO;
        }
                                              
        if(this._ehLetra()){
            while(!this._ehDelimitador()){
                this._addToken(this._ptr());
                this._incPtr();
            }
            this._tipoToken = eTiposToken.TEMPORARIO;
        }
                
        if(this._tipoToken === eTiposToken.TEMPORARIO){            
            var palavraChave = this._procurarPalavraChave();
            if(palavraChave === false){
                return eTiposToken.IDENTIFICADOR;
            }else{
                return eTiposToken.PALAVRA_CHAVE;
            }
        }
        
        return this._tipoToken;                
    },
    
    _procurarPalavraChave:function(){        
        return (PalavraChaveArray[this._tokenParaString()] !== undefined);
    },
    
    _erroDeSintaxe:function(msg){
        if(this._devDebug){
            console.log('Erro de sintaxe: ' + msg);
        }
    },
    
    _ehAlgumCaracterInformado:function(arrayCaracter){
        if(this._ehFimPrograma()){
            return false;
        }
        var caracterAtual = this._ptr();
        for(var i  = 0 ; i < arrayCaracter.length ; i++){
            if(caracterAtual === arrayCaracter.charAt(i)){
                return true;
            }
        }
        return false;
    },    
    
    _ehLetra : function(){
        if(this._ehFimPrograma()){
            return false;
        }
        return this._ptr().match(/[a-z]/i);
    },
    
    _ehNumero : function(){
        return !isNaN(this._ptr());
    },
    
    _ehDelimitador : function(){
        if(this._ehAlgumCaracterInformado(' !;,+-<>\'/*%^=()\r\n\0') || this._ptrCode() === 9){
            return true;
        }
        return false;
    },
    
    _ehEspacamento : function(){
        switch(this._ptr()){
            case ' ':
            case '\t':
                return true;
        }
        return false;
    },
            
    _ehVariavel : function(nome){        
        for(var i = 0 ; i < this._varLocal.length ; i++){
            if(this._varLocal[i]['nome'] === nome){
                return true;
            }
        }
        return false;
    },
    
    _ehVariavelInicializada : function(nome){        
        for(var i = 0 ; i < this._varLocal.length ; i++){
            if(this._varLocal[i]['nome'] === nome){
                return true;
            }
        }
        return false;
    },
           
    _setVariavel : function(nome, valor, naoTraduzirFlag){
        for(var i = 0 ; i < this._varLocal.length ; i++){
            if(this._varLocal[i]['nome'] === nome){                                                
                if(naoTraduzirFlag !== undefined){
                    this._progAssembly.push('ISTORE ');
                    this._progAssembly.push(nome);
                    this._progAssembly.push(' ; ');
                    this._progAssembly.push(valor);                    
                    this._progAssembly.push('\n');
                }
                
                this._varLocal[i]['valor'] = valor;
                return;
            }
        }
        
        // Váriável ainda não definida !                                        
        if(naoTraduzirFlag !== undefined){            
            this._progAssembly.push('ISTORE ');
            this._progAssembly.push(nome);
            this._progAssembly.push(' ; ');
            this._progAssembly.push(valor);
            this._progAssembly.push('\n');
        }
        
        this._varLocal.push({'nome': nome, 'valor' : valor});
    },
    
    _executarEscreva : function(){               
        do{
            this._encontrarToken();
           
            if(this._tipoToken !== eTiposToken.DELIMITADOR){
                this._progJava.push('\t\tSystem.out.println(');
                
                if(this._tipoToken === eTiposToken.STRING){
                    this._progJava.push('"');
                    this._progJava.push(this._token);
                    this._progJava.push('"');
                    
                    this._progAssembly.push('LDC_W CONST_');
                    this._progAssembly.push(this._posicao);
                    this._progAssembly.push(' ; \"');
                    this._progAssembly.push(this._token);
                    this._progAssembly.push('"\n');
                    
                }else if(this._tipoToken === eTiposToken.NUMERO || this._tipoToken === eTiposToken.IDENTIFICADOR){                    
                    this._progJava.push(this._token);
                    if(this._tipoToken === eTiposToken.NUMERO){
                        this._progAssembly.push('BIPUSH ');
                        this._progAssembly.push(this._token);
                        this._progAssembly.push('\n');
                    }else{
                        this._progAssembly.push('ILOAD ');
                        this._progAssembly.push(this._token);
                        this._progAssembly.push(' ; ');
                        this._progAssembly.push(this._encontrarValorVariavel(this._token));
                        this._progAssembly.push('\n');
                    }
                }
                
                this._progJava.push(');\n');
                this._progAssembly.push('OUT ; Escreve na Tela\n');
            }
                                   
        }while(!this._ehFimPrograma() && this._token !== ';');                        
    },
    
    _lacoInicio     : 0,
    _variavelLaco   : '',
    _condicaoLaco   : '',
    
    _executarPara : function(){
        this._lacoInicio = this._posicao;
        
        this._encontrarToken();        
        if(this._tipoToken === eTiposToken.IDENTIFICADOR){
            var nomeVar = this._token;
            this._variavelLaco = nomeVar;
            
            this._encontrarToken();
            
            if(this._tipoToken === eTiposToken.PALAVRA_CHAVE && this._token === 'ate'){
                                
                this._progAssembly.push('BIPUSH 0 ; Laço :: Inicialização\nISTORE ');
                this._progAssembly.push(nomeVar);
                this._progAssembly.push('\n');
                
                this._progJava.push('\t\tfor(');
                if(!this._ehVariavelInicializada(nomeVar)){                                    
                    this._progJava.push('Object ');
                }                
                this._progJava.push(nomeVar);
                this._progJava.push(' = 0 ; ');
                this._progJava.push(nomeVar);
                this._progJava.push(' < ');

                var condicao = [];
                                
                while(!this._ehFimPrograma() && this._token !== ';'){
                    this._encontrarToken();
                    
                    if(this._token !== ';'){
                        condicao.push(this._token);
                    }                                        
                }
                
                this._condicaoLaco = condicao.join('');
                this._progJava.push(this._condicaoLaco);
                
                this._progJava.push('; ');
                this._progJava.push(nomeVar);
                this._progJava.push('++){\n');
                
                this._progAssembly.push(':Laco');
                this._progAssembly.push(this._lacoInicio);
                this._progAssembly.push(' ; Laço :: Corpo\n');
                
            }else{
                this._erroDeSintaxe('Esperada palavra chave até !');
            }
        }else{
            this._erroDeSintaxe('Esperada váriavel para receber o contador !');
        }
    },
    
    _criarObjValor : function(valorInicial){
        return {'valor': valorInicial};
    },
    
    _salvarToken : function(){
        this._pilhaToken.push({'tipo' : this._tipoToken, 'token' : this._token, 'posicao' : this._posicao});
    },
    
    _devolverToken : function(){
        var ultimoToken = this._pilhaToken.pop();
        if(ultimoToken){
            this._posicao   = ultimoToken['posicao'];
            this._token     = ultimoToken['token'];
            this._tipoToken = ultimoToken['tipo'];
            
            this._log(['Token Restaurado: ', ultimoToken]);
        }else{
            this._log(['Stack Underflow :: Não exite mais elemntos na piha !']);
        }
    },
            
    _encontrarValorVariavel:function(nome){
        for(var i = 0 ; i < this._varLocal.length ; i++){
            if(this._varLocal[i]['nome'] === nome){
               return this._varLocal[i]['valor']; 
            }
        }
        return 0;
    },
    
    _encontrarValorVariavelOuNumero:function(objValor){
        this._trace(["EncontrarValorVariavelOuNumero"]);
        
        switch (this._tipoToken){
            case eTiposToken.IDENTIFICADOR:
                this._progAssembly.push('ILOAD ');
                this._progAssembly.push(this._token);
                                
                objValor['valor'] = this._encontrarValorVariavel(this._token);
                
                this._progAssembly.push(' ; ');
                this._progAssembly.push(objValor['valor']);
                this._progAssembly.push('\n');
                
                this._encontrarToken();
                break;
            case eTiposToken.NUMERO:
                objValor['valor'] = parseInt(this._token);
                
                this._progAssembly.push('BIPUSH ');
                this._progAssembly.push(this._token);
                this._progAssembly.push('\n');
                
                this._encontrarToken();
                break;
            default:
                if(this._token !== ')'){ // Expressão Vazia
                    this._erroDeSintaxe("Erro de sintaxe, experado Identificador ou Número, TipoToken: " + this._tipoToken);
                }
        }
    },
    
    _avaliarExpresaoComParenteses : function(objValor){
        this._trace(["AvaliaExpressaoComParenteses"]);
        
        if(this._token === '('){            
            this._encontrarToken();
            this._avaliarExpressaoAtribuicao(objValor);
            
            if(this._token !== ')'){
                this._erroDeSintaxe("Parênteses experado !");
            }                     
                        
            this._encontrarToken();
        }else{
            this._encontrarValorVariavelOuNumero(objValor);
        }
    },
    
    _avaliarExpressaoMaisMenosUnario : function(objValor){
        this._trace(["AvaliaExpressaoMaisMenosUnario"]);
        var operador = '\0';
        
        if(this._token === '+' || this._token === '-'){
            operador = this._token;                        
            this._encontrarToken();
        }
        
        this._avaliarExpresaoComParenteses(objValor);
        if(operador !== '\0'){
            if(operador === '-'){
                objValor['objValor'] = -(objValor['objValor']);
            }
        }     
    },
    
    _avaliarExpressaoMultiplicacaoDivisao : function(objValor){
        this._trace(["AvaliaExpressaoMultiplicacaoDivisao"]);
        
        var valorParcial = this._criarObjValor(0);
        
        this._avaliarExpressaoMaisMenosUnario(objValor);        
        while(this._token === '*' || this._token === '/' || this._token === '%'){
            var operador = this._token;
            
            this._encontrarToken();
            this._avaliarExpressaoMaisMenosUnario(valorParcial);
                                    
            if(operador === '*'){                
                objValor['valor'] *= valorParcial['valor'];
                this._progAssembly.push('IMUL ; ');
                this._progAssembly.push(objValor['valor']);
                this._progAssembly.push('\n');
            }else if(operador === '/'){
                if(valorParcial['valor'] === 0){
                    this._erroDeSintaxe('Divisão por Zero !!');
                    objValor['valor'] = 0;
                    this._progAssembly.push('IDIV ; Divisão por Zero !\n');
                }else{
                    objValor['valor'] /= valorParcial['valor'];
                    this._progAssembly.push('IDIV ; ');
                    this._progAssembly.push(objValor['valor']);
                    this._progAssembly.push('\n');
                }                
            }else if(operador === '%'){                
                if(valorParcial['valor'] === 0){
                    this._progAssembly.push('IMOD ; Divisão por Zero\n');
                    this._erroDeSintaxe('Divisão por Zero !!');
                    objValor['valor'] = 0;
                }else{                                        
                    objValor['valor'] %= valorParcial['valor'];
                    this._progAssembly.push('IMOD ; ');
                    this._progAssembly.push(objValor['valor']);
                    this._progAssembly.push('\n');
                }
            }            
        }        
    },
    
    _avaliarExpressaoSomaSubtracao : function(objValor){
        this._trace(["AvaliaExpressaoSomaSubtracao"]);
        
        var valorParcial = this._criarObjValor(0);
        
        this._avaliarExpressaoMultiplicacaoDivisao(objValor);
        while(this._token === '+' || this._token === '-'){
            var operador = this._token;
                                                         
            this._encontrarToken();
            this._avaliarExpressaoMultiplicacaoDivisao(valorParcial);
            
            if(operador === '+'){                
                objValor['valor'] += valorParcial['valor'];
                this._progAssembly.push('IADD ; ');
                this._progAssembly.push(objValor['valor']);
                this._progAssembly.push('\n');
            }else if(operador === '-'){                
                objValor['valor'] -= valorParcial['valor'];
                this._progAssembly.push('ISUB ; ');
                this._progAssembly.push(objValor['valor']);
                this._progAssembly.push('\n');
            }
        }        
    },
    
    _avaliarExpressaoAtribuicao : function(objValor){
        this._trace([">> AvaliaExpressaoAtribuição"]);
        
        if(this._tipoToken === eTiposToken.IDENTIFICADOR){            
            var nomeVariavel = this._token;
            this._encontrarToken();

            if(this._token === '='){
                
                // Para tradutor
                this._salvarPosicao();
                
                this._progJava.push('\t\t');
                if(!this._ehVariavelInicializada(nomeVariavel)){
                    this._progJava.push('private Object ');                    
                }
                this._progJava.push(nomeVariavel);
                this._progJava.push(' = ');
                while(!this._ehFimPrograma() && this._token !== ';'){
                    this._encontrarToken();
                    this._progJava.push(this._token);
                }
                this._progJava.push('\n');
                this._restaurarPosicao();
                
                this._encontrarToken();
                this._avaliarExpressaoAtribuicao(objValor);                
                this._setVariavel(nomeVariavel, objValor['valor'], true);
                return;                                   
            }else{                
                if(!this._ehVariavelInicializada(nomeVariavel)){
                    this._progJava.push('\t\tprivate Object ');
                    this._progJava.push(nomeVariavel);
                    this._progJava.push(';\n');
                    
                    this._setVariavel(nomeVariavel, 0, true);
                }                
                this._devolverToken();                    
            }
        }
        this._avaliarExpressaoSomaSubtracao(objValor);
    },
    
    _avaliarExpressao : function(objValor){
        this._trace([">> AvaliaExpressao"]);
        
        this._encontrarToken();
        
        if(this._ehFimPrograma()){
            this._erroDeSintaxe('Expressão faltando !');
            return;
        }
        
        if(this._token === ';'){
            this._log(['Expressão Vazia !']);            
            objValor['valor'] = 0;            
            return;
        }
                        
        this._avaliarExpressaoAtribuicao(objValor);
        this._devolverToken();        
    },
    
    _interpretarBloco : function(){        
        this._log(['Iniciando interpretação de bloco...']);        
        
        do{                                                
            this._encontrarToken();                        
            this._log([this._tipoToken, this._token]);            
            
            if(this._tipoToken === eTiposToken.IDENTIFICADOR){                
                var valor = this._criarObjValor(0);
                
                this._devolverToken();
                this._avaliarExpressao(valor);
                
                this._log(valor);                
            }else if(this._tipoToken === eTiposToken.PALAVRA_CHAVE){
                switch (this._token){
                    case 'escreva':
                        this._executarEscreva();
                        break;                            
                    case 'para':
                        this._executarPara();
                        break;
                    case 'fim_para':
                        this._progJava.push('\t\t}\n');
                        
                        this._progAssembly.push('ILOAD ');
                        this._progAssembly.push(this._variavelLaco);
                        this._progAssembly.push(' ; Laço :: Incremento\nBIPUSH 1\nIADD\nISTORE ');
                        this._progAssembly.push(this._variavelLaco);
                        this._progAssembly.push('\nILOAD ');
                        this._progAssembly.push(this._variavelLaco);
                        this._progAssembly.push(' ; Laço ::  Verificação \nBIPUSH ');
                        this._progAssembly.push(this._condicaoLaco);
                        this._progAssembly.push('\nIF_ICMPEQ Laco');
                        this._progAssembly.push(this._lacoInicio);
                        this._progAssembly.push(' ; Laço :: Fim\n');
                                                
                        break;
                }
            }
                                    
        }while(!this._ehFimPrograma());
        
        this._log(['Finalizando interpretação de bloco...']);        
    },
            
    _salvarPosicao : function(){
        this._salvarToken();
        this._pilhaPosicao.push(this._posicao);
    },
    
    _restaurarPosicao : function(){
        this._devolverToken();
        this._posicao = this._pilhaPosicao.pop();
    },
    
    _temPosicaoSalva : function(){
        return this._pilhaPosicao.length > 0;
    },
    
    getProgramaJava : function(){
        return this._progJava.join('');
    },
    
    getProgramaAssembly : function(){
        return this._progAssembly.join('');
    },
    
    executar : function(programa){
        this._programa = programa || '';
        if(this._programa.length > 0){
            this._programa = this._programa + '\n';
        }
        this._tamanhoPrograma = this._programa.length;
        this._posicao = 0;
        
        this._varLocal = [];
        this._token = '';
        this._pilhaToken = [];
        this._pilhaPosicao = [];
        
        this._progJava = ['public class Main {\n\n\tpublic static void main(String args[])\n'];        
        this._progAssembly = [];
        
        this._interpretarBloco();        
        
        this._progJava.push('\n\t}\n\n}');        
    }
};